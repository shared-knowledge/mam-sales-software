﻿using Mam.Sales.Core;
using Mam.Sales.Core.Models;
using Mam.Sales.Core.Receipt;
using Mam.Sales.Core.Tax;
using Mam.Sales.Core.Tax.Strategies;
using Mam.Sales.UnitTests.Helper;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Mam.Sales.UnitTests
{
    public class SalesServiceTests : TestFixtureFor<SalesService>
    {
        private static readonly AppSettings _appSettings = new AppSettings()
        {
            BasicTaxPercentage = 10,
            ImportTaxPercentage = 5
        };

        private void SetupNecessaryMockObjects()
        {
            var noTaxStrategy = GetMock<NoTaxStrategy>().Object;
            var localTaxStrategy = new LocalTaxStrategy(_appSettings, noTaxStrategy);
            var importTaxStrategy = new ImportTaxStrategy(_appSettings, localTaxStrategy);

            GetMock<IStrategyFactory>().Setup(x => x.CreateStrategies())
                                       .Returns(new Dictionary<TaxType, ITaxStrategy>()
                                       {
                                           { TaxType.NoTax, GetMock<NoTaxStrategy>().Object},
                                           { TaxType.LocalTax, localTaxStrategy},
                                           { TaxType.ImportTax, importTaxStrategy}
                                       });


            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.NoTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.NoTax]);

            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.LocalTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.LocalTax]);

            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.ImportTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.ImportTax]);
        }

        protected override SalesService ConstructSystemUnderTest()
        {
            SetupNecessaryMockObjects();

            return new SalesService(
                GetMock<ReceiptHandler>().Object,
                GetMock<IStrategyResolver>().Object,
                GetMock<TaxCalculator>().Object,
                _appSettings);
        }

        [Fact]
        public void Process_Input_Order_1()
        {
            // Arrange
            var book = new Products()
            {
                Name = "Book",
                Price = 12.49m,
                Taxable = false,
                Imported = false
            };
            var musicCd = new Products()
            {
                Name = "Music CD",
                Price = 14.99m,
                Taxable = true,
                Imported = false
            };
            var chocolateBar = new Products()
            {
                Name = "Chocolate bar",
                Price = 0.85m,
                Taxable = false,
                Imported = false
            };

            var order = new Order() { Products = new List<Products>() { book, musicCd, chocolateBar } };

            /* ================= Expected Outputs ======================= */
            var bookPriceWithTax = 12.49m;
            var musicCdPriceWithTax = 16.49m;
            var chocolateBarPriceWithTax = 0.850m;

            var expectedTotalTax = 1.50m;
            var expectedTotalSalesCost = 29.83m;

            // Act
            Receipt receipt = Sut.ProcessOrder(order);

            var bookReceipt = receipt.Receipts.First(x => x.Name == book.Name);
            var musicCdReceipt = receipt.Receipts.First(x => x.Name == musicCd.Name);
            var chocolateBarReceipt = receipt.Receipts.First(x => x.Name == chocolateBar.Name);

            // Assert
            Assert.NotNull(receipt);

            Assert.Equal(bookPriceWithTax, bookReceipt.Price);
            Assert.Equal(musicCdPriceWithTax, musicCdReceipt.Price);
            Assert.Equal(chocolateBarPriceWithTax, chocolateBarReceipt.Price);

            Assert.Equal(expectedTotalTax, receipt.TotalSalesTax);
            Assert.Equal(expectedTotalSalesCost, receipt.TotalSalesCost);
        }

        [Fact]
        public void Process_Input_Order_2()
        {
            // Arrange
            var chocolate = new Products()
            {
                Name = "Imported box of chocolates",
                Price = 10.00m,
                Taxable = false,
                Imported = true
            };
            var perfume = new Products()
            {
                Name = "Imported bottle of perfume",
                Price = 47.50m,
                Taxable = true,
                Imported = true
            };

            var order = new Order() { Products = new List<Products>() { chocolate, perfume } };

            /* ================= Expected Outputs ======================= */
            var chocolateBoxPriceWithTax = 10.50m;
            var perfumeBottlePriceWithTax = 54.65m;

            var expectedTotalTax = 7.65m;
            var expectedTotalSalesCost = 65.15m;

            // Act
            Receipt receipt = Sut.ProcessOrder(order);

            var chocolateReceipt = receipt.Receipts.First(x => x.Name == chocolate.Name);
            var perfumeReceipt = receipt.Receipts.First(x => x.Name == perfume.Name);

            // Assert
            Assert.NotNull(receipt);

            Assert.Equal(chocolateBoxPriceWithTax, chocolateReceipt.Price );
            Assert.Equal(perfumeBottlePriceWithTax, perfumeReceipt.Price);

            Assert.Equal(expectedTotalTax, receipt.TotalSalesTax);
            Assert.Equal(expectedTotalSalesCost, receipt.TotalSalesCost);
        }

        [Fact]
        public void Process_Input_Order_3()
        {
            // Arrange
            var importedPerfume = new Products()
            {
                Name = "imported bottle of perfume",
                Price = 27.99m,
                Taxable = true,
                Imported = true
            };
            var perfume = new Products()
            {
                Name = "bottle of perfume",
                Price = 18.99m,
                Taxable = true,
                Imported = false
            };
            var headachePills = new Products()
            {
                Name = "packet of headache pills",
                Price = 9.75m,
                Taxable = false,
                Imported = false
            };
            var importedChocolate = new Products()
            {
                Name = "imported box of chocolates",
                Price = 11.25m,
                Taxable = false,
                Imported = true
            };

            var order = new Order() { Products = new List<Products>() { importedPerfume, perfume, headachePills, importedChocolate } };

            /* ================= Expected Outputs ======================= */
            var importedPerfumePriceWithTax = 32.19m;
            var perfumePriceWithTax = 20.89m;
            var headachePillsPriceWithTax = 9.75m;
            var importedChocolatePriceWithTax = 11.85m;

            var expectedTotalTax = 6.70m;
            var expectedTotalSalesCost = 74.68m;

            // Act
            Receipt receipt = Sut.ProcessOrder(order);

            var importedPerfumeReceipt = receipt.Receipts.First(x => x.Name == importedPerfume.Name);
            var perfumeReceipt = receipt.Receipts.First(x => x.Name == perfume.Name);
            var headachePillsReceipt = receipt.Receipts.First(x => x.Name == headachePills.Name);
            var importedChocolateReceipt = receipt.Receipts.First(x => x.Name == importedChocolate.Name);

            // Assert
            Assert.NotNull(receipt);

            Assert.Equal(importedPerfumePriceWithTax, importedPerfumeReceipt.Price);
            Assert.Equal(perfumePriceWithTax, perfumeReceipt.Price);
            Assert.Equal(headachePillsPriceWithTax, headachePillsReceipt.Price);
            Assert.Equal(importedChocolatePriceWithTax, importedChocolateReceipt.Price);

            Assert.Equal(expectedTotalTax, receipt.TotalSalesTax);
            Assert.Equal(expectedTotalSalesCost, receipt.TotalSalesCost);
        }
    }
}
