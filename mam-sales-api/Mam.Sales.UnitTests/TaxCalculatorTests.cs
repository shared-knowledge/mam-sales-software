﻿using Mam.Sales.Core;
using Mam.Sales.Core.Models;
using Mam.Sales.Core.Tax;
using Mam.Sales.Core.Tax.Strategies;
using Mam.Sales.UnitTests.Helper;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace Mam.Sales.UnitTests
{
    public class TaxCalculatorTests : TestFixtureFor<TaxCalculator>
    {
        private static readonly AppSettings _appSettings = new AppSettings()
        {
            BasicTaxPercentage = 10,
            ImportTaxPercentage = 5
        };

        private void SetupNecessaryMockObjects()
        {
            var noTaxStrategy = GetMock<NoTaxStrategy>().Object;
            var localTaxStrategy = new LocalTaxStrategy(_appSettings, noTaxStrategy);
            var importTaxStrategy = new ImportTaxStrategy(_appSettings, localTaxStrategy);

            GetMock<IStrategyFactory>().Setup(x => x.CreateStrategies())
                                       .Returns(new Dictionary<TaxType, ITaxStrategy>()
                                       {
                                           { TaxType.NoTax, GetMock<NoTaxStrategy>().Object},
                                           { TaxType.LocalTax, localTaxStrategy},
                                           { TaxType.ImportTax, importTaxStrategy}
                                       });


            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.NoTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.NoTax]);

            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.LocalTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.LocalTax]);

            GetMock<IStrategyResolver>().Setup(x => x.ResolveStrategyFor(TaxType.ImportTax))
                                        .Returns(GetMock<IStrategyFactory>().Object.CreateStrategies()[TaxType.ImportTax]);
        }

        protected override TaxCalculator ConstructSystemUnderTest()
        {
            SetupNecessaryMockObjects();

            return new TaxCalculator();
        }

        [Fact]
        public void Process_UnTaxable_Local_Order()
        {
            // Arrange
            var product = new Products()
            {
                Name = "Book",
                Price = 12.49m,
                Taxable = false,
                Imported = false
            };

            Sut.SetStrategy(GetMock<IStrategyResolver>().Object.ResolveStrategyFor(TaxType.NoTax));

            // Act
            Products taxedProduct = Sut.ApplyTax(product);
            var expectedProductPriceWithTax = 12.49m;

            // Assert
            Assert.NotNull(taxedProduct);
            Assert.Equal(expectedProductPriceWithTax, taxedProduct.TaxedPrice);
        }

        [Fact]
        public void Process_Taxable_Local_Order()
        {
            // Arrange
            var product = new Products()
            {
                Name = "music CD",
                Price = 14.99m,
                Taxable = true,
                Imported = false
            };

            Sut.SetStrategy(GetMock<IStrategyResolver>().Object.ResolveStrategyFor(TaxType.LocalTax));

            // Act
            Products taxedProduct = Sut.ApplyTax(product);
            var expectedProductPriceWithTax = 16.49m;

            // Assert
            Assert.NotNull(taxedProduct);
            Assert.Equal(expectedProductPriceWithTax, taxedProduct.TaxedPrice);
        }

        [Fact]
        public void Process_UnTaxable_Import_Order()
        {
            // Arrange
            var product = new Products()
            {
                Name = "Imported box of chocolates",
                Price = 10.00m,
                Taxable = false,
                Imported = true
            };

            Sut.SetStrategy(GetMock<IStrategyResolver>().Object.ResolveStrategyFor(TaxType.ImportTax));

            // Act
            Products taxedProduct = Sut.ApplyTax(product);
            var expectedProductPriceWithTax = 10.50m;

            // Assert
            Assert.NotNull(taxedProduct);
            Assert.Equal(expectedProductPriceWithTax, taxedProduct.TaxedPrice);
        }

        [Fact]
        public void Process_Taxable_Import_Order()
        {
            // Arrange
            var product = new Products()
            {
                Name = "Imported bottle of perfume",
                Price = 47.50m,
                Taxable = true,
                Imported = true
            };

            Sut.SetStrategy(GetMock<IStrategyResolver>().Object.ResolveStrategyFor(TaxType.ImportTax));

            // Act
            Products taxedProduct = Sut.ApplyTax(product);
            var expectedProductPriceWithTax = 54.65m;

            // Assert
            Assert.NotNull(taxedProduct);
            Assert.Equal(expectedProductPriceWithTax, taxedProduct.TaxedPrice);
        }

        [Fact]
        public void Process_Multiple_Taxable_Import_Order()
        {
            // Arrange
            var localUnTaxableProduct = new Products(quantity: 5)
            {
                Name = "Imported bottle of perfume",
                Price = 27.99m,
                Taxable = true,
                Imported = true
            };

            Sut.SetStrategy(GetMock<IStrategyResolver>().Object.ResolveStrategyFor(TaxType.ImportTax));

            // Act
            Products taxedProduct = Sut.ApplyTax(localUnTaxableProduct);
            var expectedProductPriceWithTax = 32.19m * localUnTaxableProduct.Quantity;

            // Assert
            Assert.NotNull(taxedProduct);
            Assert.Equal(expectedProductPriceWithTax, taxedProduct.TaxedPrice);
        }
    }
}
