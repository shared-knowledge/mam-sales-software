﻿using Moq;
using System;
using System.Collections.Generic;

namespace Mam.Sales.UnitTests.Helper
{
    public abstract class TestFixtureFor<TSut> where TSut : class
    {
        private TSut _sut;
        private readonly Dictionary<Type, object> _mocks = new Dictionary<Type, object>();


        protected TSut Sut => _sut ?? (_sut = ConstructSystemUnderTest());
        protected abstract TSut ConstructSystemUnderTest();


        protected Mock<TDependency> GetMock<TDependency>() where TDependency : class
        {
            var mock = new Mock<TDependency>();

            if (_mocks.ContainsKey(typeof(TDependency)))
                return _mocks[typeof(TDependency)] as Mock<TDependency>;

            _mocks.Add(typeof(TDependency), mock);

            return mock;
        }
    }
}
