﻿using Mam.Sales.Core;
using Mam.Sales.Core.Receipt;
using Mam.Sales.Core.Tax;
using Mam.Sales.Core.Tax.Strategies;
using Microsoft.Extensions.DependencyInjection;

namespace Mam.Sales.Api
{
    public static class MyServiceCollectionExtensions
    {
        public static IServiceCollection AddMyCoreServices(this IServiceCollection services)
        {
            services.AddTaxStrategyServices();

            services.AddScoped<ITaxCalculator, TaxCalculator>();
            services.AddScoped<ISalesService, SalesService>();
            services.AddScoped<IReceiptHandler, ReceiptHandler>();

            return services;
        }

        public static IServiceCollection AddTaxStrategyServices(this IServiceCollection services)
        {
            services.AddSingleton<ITaxStrategy, NoTaxStrategy>();
            services.AddSingleton<ITaxStrategy, LocalTaxStrategy>();
            services.AddSingleton<ITaxStrategy, ImportTaxStrategy>();

            services.AddSingleton<IStrategyFactory, StrategyFactory>();
            services.AddSingleton<IStrategyResolver, StrategyResolver>();

            return services;
        }
    }
}
