﻿using Mam.Sales.Api.Command;
using Mam.Sales.Core;
using Mam.Sales.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mam.Sales.Api.Controllers
{
    public class SalesController: Controller
    {
        readonly ILogger<SalesController> _logger;
        readonly ISalesService _salesService;

        public SalesController(ISalesService salesService, ILogger<SalesController> logger)
        {
            _logger = logger;
            _salesService = salesService;
        }

        [Route("sales/order")]
        [HttpPost]
        public IActionResult SalesOrder([FromBody] PlaceOrderCommand command)
        {
            return Ok(_salesService.ProcessOrder(command.MapToOrderModel()));
        }
    }
}
