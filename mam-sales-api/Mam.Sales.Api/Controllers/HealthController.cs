﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mam.Sales.Api.Controllers
{
    public class HealthController : Controller
    {
        readonly ILogger<HealthController> _logger;

        public HealthController(ILogger<HealthController> logger)
        {
            _logger = logger;
        }

        [Route("api/health")]
        [HttpGet]
        public IActionResult Health()
        {
            var healthMsg = "success :: api available";

            _logger.LogInformation(healthMsg);
            return Ok(healthMsg);
        }
    }
}
