﻿using Mam.Sales.Core.Exceptions;
using Mam.Sales.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mam.Sales.Api.Command
{
    public class PlaceOrderCommand
    {
        public List<OrderCommandProducts> Products { get; set; }
        public string DiscountCode { get; set; }

        public Order MapToOrderModel()
        {
            if (Products == null || Products.Count <= 0)
                throw new BadRequestException($"{nameof(PlaceOrderCommand)} failed :: You need to provide atleast one product to place an order.");

            return new Order()
            {
                Products = Products.Select(x => 
                new Products()
                {
                    Name = x.Name,
                    Price = (decimal)x.Price,
                    Taxable = x.Taxable,
                    Imported = x.Imported,
                    Quantity = x.Quantity
                }).ToList(),
                DiscountCode = DiscountCode
            };
        }
    }

    public class OrderCommandProducts
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public bool Taxable { get; set; }
        public bool Imported { get; set; }
        public decimal Quantity { get; set; }
    }
}
