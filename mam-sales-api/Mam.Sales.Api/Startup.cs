﻿using System.IO;
using Mam.Sales.Core;
using Mam.Sales.Core.Extensions;
using Mam.Sales.Core.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.PlatformAbstractions;

namespace Mam.Sales.Api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public Startup(IHostingEnvironment hostEnv)
        {
            Configuration = new ConfigurationBuilder().WithConfiguration(hostEnv).Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions<AppSettings>();
            services.Configure<AppSettings>(Configuration.GetSection("appSettings"));
            services.AddTransient((s) => s.GetRequiredService<IOptions<AppSettings>>().Value);

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins,
                builder =>
                {
                    builder.WithOrigins(Configuration.GetSection("appSettings").Get<AppSettings>().SalesWebAppAddress)
                           .AllowAnyHeader()
                           .AllowAnyMethod();
                });
            });

            services.AddMyCoreServices();
            services.AddMvc();
            services.AddSwaggerDocumentationService();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCors(MyAllowSpecificOrigins);
            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseMvc();
            app.UseSwagger();

            loggerFactory.AddFile(Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, @"Logs\log.txt"));

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mam.Sales Api");
                c.RoutePrefix = string.Empty;
            });
        }
    }
}
