﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Models
{
    public class Order
    {
        public List<Products> Products { get; set; }

        /// <summary>
        /// DiscountCode :: Hypothetical property to show why you'd want an "Órder' object as opposed to just pass a products list around
        /// </summary>
        public string DiscountCode { get; set; }
    }
}
