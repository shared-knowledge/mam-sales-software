﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Models
{
    public class Products
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public bool Taxable { get; set; }
        public bool Imported { get; set; }
        public decimal Quantity { get; set; }

        public decimal SalesTax { get; set; }
        public decimal TaxedPrice { get; set; }

        public Products(int quantity=1)
        {
            Quantity = quantity;
        }
    }
}
