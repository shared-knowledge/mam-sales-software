﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Models
{
    public class Receipt
    {
        public List<ProductReceipts> Receipts { get; set; }
        public decimal TotalSalesTax { get; set; }
        public decimal AppliedDiscount { get; set; }
        public decimal TotalSalesCost { get; set; }
    }

    public class ProductReceipts
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal SalesTax { get; set; }
        public int Quantity { get; set; }
    }
}
