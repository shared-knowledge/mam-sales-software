﻿using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Extensions
{
    public static class SwaggerDocumentationServicesExtensions
    {
        public static IServiceCollection AddSwaggerDocumentationService(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Sample Api for Mam.Sales Application", Version = "v1" });
            });

            return services;
        }
    }
}
