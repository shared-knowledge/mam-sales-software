﻿using Mam.Sales.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Extensions
{
    public static class ReceiptsExtensions
    {
        public static string GenerateReceiptString(this Models.Receipt receipt)
        {
            string receiptString = "\n" +
            "\n --------------------- MAM Sales Order Receipt --------------------- \n";
            receiptString += $" \n * DATE  :: {DateTime.UtcNow.ToString()} \n";

            foreach (ProductReceipts product in receipt.Receipts)
            {
                receiptString += $" * {product.Quantity} {product.Name} :: {product.Price}"+"\n";
            }

            receiptString += $" \n ";
            receiptString += " * Sales Taxes   :: " + receipt.TotalSalesTax + "\n";
            receiptString += " * Total         ::  " + receipt.TotalSalesCost + "\n";

            receiptString += $" \n " +
            $"\n ------------------------------------------------------------------ " + "\n ";

            return receiptString;
        }
    }
}
