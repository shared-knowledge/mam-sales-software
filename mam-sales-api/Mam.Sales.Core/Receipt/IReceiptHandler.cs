﻿using Mam.Sales.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Receipt
{
    public interface IReceiptHandler
    {
        Models.Receipt Generate(Order order);
    }
}
