﻿using System;
using System.Collections.Generic;
using System.Text;
using Mam.Sales.Core.Models;

namespace Mam.Sales.Core.Receipt
{
    public class ReceiptHandler : IReceiptHandler
    {
        public Models.Receipt Generate(Order order)
        {
            List<ProductReceipts> receipts = new List<ProductReceipts>();

            decimal totalSalesTaxes = 0.0m;
            decimal totalSalesCosts = 0.0m;

            foreach(var product in order.Products)
            {
                totalSalesCosts += product.Price + product.SalesTax;
                totalSalesTaxes += product.SalesTax;

                receipts.Add(new ProductReceipts()
                {
                    Name = product.Name,
                    Price = product.TaxedPrice,
                    SalesTax = product.SalesTax,
                    Quantity = (int)product.Quantity
                });
            }

            return new Models.Receipt()
            {
                Receipts = receipts,
                TotalSalesCost = totalSalesCosts,
                TotalSalesTax = totalSalesTaxes
            };
        }
    }
}
