﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core
{
    public class AppSettings
    {
        public int BasicTaxPercentage { get; set; }
        public int ImportTaxPercentage { get; set; }
        public string SalesWebAppAddress { get; set; }
    }
}
