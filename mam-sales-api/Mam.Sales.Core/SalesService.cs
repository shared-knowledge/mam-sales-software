﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mam.Sales.Core.Models;
using Mam.Sales.Core.Receipt;
using Mam.Sales.Core.Tax;
using Mam.Sales.Core.Tax.Strategies;

namespace Mam.Sales.Core
{
    public class SalesService : ISalesService
    {
        private readonly AppSettings _appSettings;
        private readonly ITaxCalculator _taxCalculator;
        private readonly IReceiptHandler _receiptHandler;
        private readonly IStrategyResolver _strategyResolver;

        public SalesService(
            IReceiptHandler receiptHandler, 
            IStrategyResolver strategyResolver,
            ITaxCalculator taxCalculator, 
            AppSettings appSettings)
        {
            _appSettings = appSettings;
            _taxCalculator = taxCalculator;
            _receiptHandler = receiptHandler;
            _strategyResolver = strategyResolver;
        }

        public Models.Receipt ProcessOrder(Order order)
        {
            var taxedOrder = ApplyProductTax(order);
            var receipt = _receiptHandler.Generate(taxedOrder);

            return receipt;
        }

        private Order ApplyProductTax(Order order)
        {
            List<Products> taxedProducts = new List<Products>();

            foreach (Products product in order.Products)
            {
                if (product.Imported)
                {
                    _taxCalculator.SetStrategy(_strategyResolver.ResolveStrategyFor(TaxType.ImportTax));
                    taxedProducts.Add(_taxCalculator.ApplyTax(product));
                }

                if (!product.Imported)
                {
                    _taxCalculator.SetStrategy(_strategyResolver.ResolveStrategyFor(TaxType.LocalTax));
                    taxedProducts.Add(_taxCalculator.ApplyTax(product));
                }
            }

            order.Products = taxedProducts;
            return order;
        }
    }
}
