﻿using Mam.Sales.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core
{
    public interface ISalesService
    {
        Models.Receipt ProcessOrder(Order order);
    }
}
