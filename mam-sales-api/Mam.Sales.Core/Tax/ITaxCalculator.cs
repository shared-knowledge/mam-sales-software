﻿using Mam.Sales.Core.Models;
using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public interface ITaxCalculator
    {
        void SetStrategy(ITaxStrategy taxStrategy);
        Products ApplyTax(Products product);
    }
}
