﻿using Mam.Sales.Core.Models;
using System;

namespace Mam.Sales.Core.Tax.Strategies
{
    public class NoTaxStrategy : TaxStrategy
    {
        public override decimal CalculateTax(Products product)
        {
            return 0.00m;
        }
    }
}
