﻿using Mam.Sales.Core.Models;
using System;

namespace Mam.Sales.Core.Tax.Strategies
{
    public class ImportTaxStrategy : LocalTaxStrategy
    {
        readonly AppSettings _appSettings;
        public ImportTaxStrategy(AppSettings appSettings, NoTaxStrategy taxStrategy) : base(appSettings, taxStrategy)
        {
            _appSettings = appSettings;
        }

        public override decimal CalculateTax(Products product)
        {
            decimal importTaxPercentage = ((decimal)_appSettings.ImportTaxPercentage) / 100;
            var importTaxDeducted = product.Price * importTaxPercentage;

            return !product.Taxable ? importTaxDeducted :  base.CalculateTax(product) + importTaxDeducted;
        }
    }
}
