﻿using Mam.Sales.Core.Models;
using System;

namespace Mam.Sales.Core.Tax.Strategies
{
    public class LocalTaxStrategy : NoTaxStrategy
    {
        readonly AppSettings _appSettings;
        readonly NoTaxStrategy _taxStrategy;

        public LocalTaxStrategy(AppSettings appSettings, NoTaxStrategy taxStrategy)
        {
            _appSettings = appSettings;
            _taxStrategy = taxStrategy;
        }

        public override decimal CalculateTax(Products product)
        {
            if (!product.Taxable)
                return _taxStrategy.CalculateTax(product);

            decimal taxPercentage = ((decimal)_appSettings.BasicTaxPercentage) / 100;

            return product.Price * taxPercentage;
        }
    }
}
