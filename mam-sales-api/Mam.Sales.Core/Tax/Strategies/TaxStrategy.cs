﻿using Mam.Sales.Core.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax.Strategies
{
    public interface ITaxStrategy
    {
        decimal CalculateTax(Products product);
    }

    public abstract class TaxStrategy : ITaxStrategy
    {
        public abstract decimal CalculateTax(Products product);
    }
}
