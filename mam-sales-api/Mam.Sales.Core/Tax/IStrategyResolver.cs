﻿using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public interface IStrategyResolver
    {
        ITaxStrategy ResolveStrategyFor(TaxType strategy);
    }
}
