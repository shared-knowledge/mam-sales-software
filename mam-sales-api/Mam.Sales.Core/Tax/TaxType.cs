﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public enum TaxType
    {
        NoTax,
        LocalTax,
        ImportTax
    }
}
