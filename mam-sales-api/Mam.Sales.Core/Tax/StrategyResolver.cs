﻿using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public class StrategyResolver : IStrategyResolver
    {
        private readonly IDictionary<TaxType, ITaxStrategy> _taxStrategies;

        public  StrategyResolver(IStrategyFactory strategyFactory)
        {
            _taxStrategies = strategyFactory.CreateStrategies();
        }


        public ITaxStrategy ResolveStrategyFor(TaxType strategy)
        {
            return _taxStrategies[strategy] ?? throw new ArgumentNullException($"UnSupported Tax Strategy with Type :: {strategy.ToString()}");
        }
    }
}
