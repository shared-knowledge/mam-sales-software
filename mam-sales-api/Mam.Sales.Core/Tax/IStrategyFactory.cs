﻿using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public interface IStrategyFactory
    {
        IDictionary<TaxType, ITaxStrategy> CreateStrategies();
    }
}
