﻿using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public class StrategyFactory : IStrategyFactory
    {
        public IDictionary<TaxType, ITaxStrategy> TaxStrategies { get; set; }

        private readonly NoTaxStrategy _noTaxStrategy;
        private readonly LocalTaxStrategy _localTaxStrategy;
        private readonly ImportTaxStrategy _importTaxStrategy;

        private readonly AppSettings _appSettings;

        public StrategyFactory(AppSettings appSettings)
        {
            _appSettings = appSettings;

            _noTaxStrategy = new NoTaxStrategy();
            _localTaxStrategy = new LocalTaxStrategy(_appSettings, _noTaxStrategy);
            _importTaxStrategy = new ImportTaxStrategy(_appSettings, _localTaxStrategy);
        }

        public IDictionary<TaxType, ITaxStrategy> CreateStrategies()
        {
            return new Dictionary<TaxType, ITaxStrategy>()
            {
                {TaxType.NoTax, _noTaxStrategy},
                {TaxType.LocalTax, _localTaxStrategy},
                {TaxType.ImportTax, _importTaxStrategy}
            };
        }
    }
}
