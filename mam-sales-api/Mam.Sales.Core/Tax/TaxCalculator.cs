﻿using Mam.Sales.Core.Models;
using Mam.Sales.Core.Tax.Strategies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mam.Sales.Core.Tax
{
    public class TaxCalculator : ITaxCalculator
    {
        private ITaxStrategy _taxStrategy;

        public void SetStrategy(ITaxStrategy taxStrategy)
        {
            _taxStrategy = taxStrategy;
        }

        public Products ApplyTax(Products product)
        {
            decimal unitTax = _taxStrategy.CalculateTax(product);

            product.SalesTax = RoundTax(unitTax * product.Quantity);

            product.TaxedPrice = product.SalesTax + (product.Price * product.Quantity);

            return product;
        }

        private decimal RoundTax(decimal value)
        {
            var ceiling = Math.Ceiling(value * 20);

            return (ceiling == 0) ? 0 : ceiling / 20;
        }
    }
}
