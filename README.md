# mam-sales-software

Mam Sales Api written in .net core 2.2 and Web Application written utilizing vue.js 2.6

## Folder Structure
- mam-sales-api         :: .NET Core Api 
- mam-sales-webapp      :: vue.js web app

## Run Web Api
- From root directory of git folder, 
    - cd .\mam-sales-api\
- Fire up api solution using visual studio
- Run using Mam.Sales.Api profile 
	- serves at :: https://localhost:5021/; http://localhost:5020/
	- swagger page at :: https://localhost:5021/index.html ; http://localhost:5020/index.html

- Note:
    - If you chose to Run with the default profile which is IISExpress, it will not serve on port '5020/5021', instead it'd serve on port '44359'. Thus served at https://localhost:44359/. To use the web app with this, adjust url port in http.js file at path './mam-sales-webapp/src/http.js' from root directory of git


## Run Web App
- From root directory of git folder, 
    - cd .\mam-sales-webapp\
- npm i
- npm run serve
    - serves web app at :: http://localhost:8080/


## Test Api using the Web App

- Run api with method of choice : IISExpress, Mam.Sales.Api profile or Docker
- Run webapp with method of choice : npm run serve or Docker ( using npm run docker-build then npm docker-up. Note that thsis requires docker installed on your local)
- On web app, select product by click. 
- Click on "Process Cart Order (Add tax)" Button to display Order Receipt with added tax in a Modal. See Images for example output



## Test Api endpoints directly, using postman / fiddler etc...;

- endpoint : api/health
    - VERB :: GET
    - parameters :: null
    - response
    ```
    success :: api available
    ```

- endpoint : sales/order
    - VERB :: POST
    - payload :: 
    ```
    {
        "products": [],
        "discountCode": ""
    }
    ```
    - sample payload for input 3
    ``` 
    {
        "products": [        
            { "name": "imported bottle of perfume", "price": 27.99, "taxable": true, "imported": true, "quantity": 1 },
            { "name": "bottle of perfume", "price": 18.99, "taxable": true, "imported": false, "quantity": 1 },
            { "name": "packet of headache pills", "price": 9.75, "taxable": false, "imported": false, "quantity": 1 },
            { "name": "imported box of chocolates", "price": 11.25, "taxable": false, "imported": true, "quantity": 1 }
        ]
    }
    ```

- response for input 3
```
{
    "receipts": [
        {
            "name": "imported bottle of perfume",
            "price": 32.19,
            "salesTax": 4.2,
            "quantity": 1
        },
        {
            "name": "bottle of perfume",
            "price": 20.89,
            "salesTax": 1.9,
            "quantity": 1
        },
        {
            "name": "packet of headache pills",
            "price": 9.75,
            "salesTax": 0.0,
            "quantity": 1
        },
        {
            "name": "imported box of chocolates",
            "price": 11.85,
            "salesTax": 0.6,
            "quantity": 1
        }
    ],
    "totalSalesTax": 6.7,
    "appliedDiscount": 0.0,
    "totalSalesCost": 74.68
}
```

## Bonus - Docker
- You'd need to have docker and docker-compose installed with docker running on your local to proceed

- Run Web App with Docker 
    - From root directory of git folder, 
        - cd .\mam-sales-webapp\
    - npm run docker-build
    - npm run docker-up
        - serves web app at :: http://localhost:8000/

- Run Web Api with Docker 
    - From root directory of git folder, 
        - cd .\mam-sales-api\
    - Fire up api solution using visual studio
    - Set docker-compose as startup project
        - This would begin building your images in the background.
        - As a part of the image build steps, all unit test are run and image build only completes on success of all unit tests
        - Inspect Dockerfile at '.\mam-sales-api\Mam.Sales.Api\Dockerfile' from directory root to see image build steps
    - Run docker-compose
        - Serves api at http://localhost:5020 from Docker Images built in previous step