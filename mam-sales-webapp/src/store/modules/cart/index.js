import axios from '../../../http';

const state = {
    cartItems: [],
    cartReceipt: [],
    totalSalesTax: 0.0,
    totalSalesCost: 0.0,
    processingCartOrder: false,
    processingCartOrderSuccess: false
};

const mutations = {
    PROCESSING_ORDER (state) {
        state.processingCartOrder = true;
    },
    PROCESSING_ORDER_DONE (state, success) {
        state.processingCartOrder = false;
        state.processingCartOrderSuccess = success;
    },
    UPDATE_CART_RECEIPT (state, payload) {

        for (var i = 0; i < payload.receipts.length; i++) {
            var rcpt = payload.receipts[i];
            state.cartReceipt.push(rcpt);
            console.log(rcpt);
        }
        
        state.totalSalesTax = payload.totalSalesTax;
        state.totalSalesCost = payload.totalSalesCost;
    },
    ADD_CART_ITEM (state, payload) {

        let productFoundInCart = state.cartItems.find(
            item => item.id === payload.id
        );

        if (productFoundInCart) {
            productFoundInCart.quantity++
        } else {
            payload.quantity++
            state.cartItems.push(payload)
        }
    },
    REMOVE_CART_ITEM (state, payload) {

        let productFoundInCart = state.cartItems.find(
            item => item.id === payload.id
        );

        if (productFoundInCart) {
            productFoundInCart.quantity--
            if (productFoundInCart.quantity === 0) {
                state.cartItems = state.cartItems.filter(item => item.id !== payload.id)
            }
        }
    },
    CLEAR_CART (state) {
        state.cartReceipt = [];
        state.totalSalesTax = 0.0;
        state.totalSalesCost = 0.0;
    }
};

const actions = {
    addCartItem ( { commit }, product ) {
        commit('ADD_CART_ITEM', product);
    },
    removeCartItem ( { commit }, product ) {
        commit('REMOVE_CART_ITEM', product);
    },
    processCartOrder ({ commit }, order) {
        commit('CLEAR_CART');
        let payload = {
            products: order
        }
        axios.post('sales/order', payload )
             .then((response) => {
                commit('PROCESSING_ORDER_DONE', true);
                commit('UPDATE_CART_RECEIPT', response.data);
             })
             .catch((error) => {
                console.log(error);
                 commit('PROCESSING_ORDER_DONE', error, true);
             });
    }
};

const getters = {
    cartItems : state => state.cartItems,
    cartQuantity : state => {
        return state.cartItems.reduce((acc, cartItem) => {
            return cartItem.quantity + acc;
        }, 0)
    },
    cartUnTaxedTotal : state => {
        return state.cartItems.reduce((acc, cartItem) => {
            return (cartItem.quantity * cartItem.price) + acc;
        },0).toFixed(2);
    },
    processingCartOrder : state => state.processingCartOrder,
    processingCartOrderSuccess : state => state.processingCartOrderSuccess,
    cartReceipt : state => state.cartReceipt,
    totalSalesTax : state => state.totalSalesTax,
    totalSalesCost :  state => state.totalSalesCost

};

const cartModule = {
    state,
    mutations,
    actions,
    getters
};

export default cartModule;