const productSeedData = [
    { id: 1, name: "Book", price: 12.49, taxable: false, imported: false, quantity: 0 },
    { id: 2, name: "Music CD", price: 14.99, taxable: true, imported: false, quantity: 0 },
    { id: 3, name: "Chocolate Bar", price: 0.85, taxable: false, imported: false, quantity: 0 },
    { id: 4, name: "Imported box of chocolates", price: 10.00, taxable: false, imported: true, quantity: 0 },
    { id: 5, name: "Imported bottle of perfume ", price: 47.50, taxable: true, imported: true, quantity: 0 },
    { id: 6, name: "Imported bottle of perfume", price: 27.99, taxable: true, imported: true, quantity: 0 },
    { id: 7, name: "Bottle of perfume", price: 18.99, taxable: true, imported: false, quantity: 0 },
    { id: 8, name: "Packet of paracetamol", price: 9.75, taxable: false, imported: false, quantity: 0 },
    { id: 9, name: "Box of imported chocolates", price: 11.25, taxable: false, imported: true, quantity: 0 }
]

export default productSeedData;