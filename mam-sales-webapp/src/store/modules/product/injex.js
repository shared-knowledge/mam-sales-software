import productSeedData from './product-seed.js';

const state = {
    products : productSeedData
}

const getters = {
    products: state => state.products,
};

const productModule = {
    state,
    getters
};

export default productModule;