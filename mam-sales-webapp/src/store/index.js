import Vue from 'vue';
import Vuex from 'vuex';

import cart from './modules/cart';
import product from './modules/product/injex.js';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        product,
        cart
    }
});