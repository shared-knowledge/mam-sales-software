import axios from 'axios';

const baseURL = "http://localhost:5020";

const axiosInstance = axios.create({
    baseURL,
    headers: {
        'Content-type': 'application/json'
    }
});

export default axiosInstance